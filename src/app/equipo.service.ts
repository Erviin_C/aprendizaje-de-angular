import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EquipoService {

  equipo:any[] = [
    {
      nombre: 'Ervin',
      cedula: '1004071732',
      correo: 'ervin.cabascango.s@gmail.com'
    },
    {
      nombre: 'Steven',
      cedula: '1002003000',
      correo: 'steven524.s@gmail.com'
    }
  ]

  constructor() { 
    console.log('funcionando');
    
  }


  obtenerEquipo(){
    return this.equipo;
  }

  obtenerUno(i){
    return this.equipo[i];
  }

}
